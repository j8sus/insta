class User < ApplicationRecord
	has_many :photos, dependent: :destroy
	has_many :likes, dependent: :destroy
	has_many :comments, dependent: :destroy

  has_many :followers, :class_name => 'Follower', :foreign_key => 'person_id'
  has_many :following, :class_name => 'Follower', :foreign_key => 'follower_id'
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :image,
                    styles: { medium: '300x300>', avatar: '72x72'},
                    default_url: '/images/:style/missing.png'
  validates_attachment_content_type :image, 
                    content_type: ['image/jpeg', 'image/gif', 'image/png']
end