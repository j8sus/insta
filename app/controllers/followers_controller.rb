class FollowersController < ApplicationController
  def create
    unless user_signed_in?
      redirect_to new_user_session_path
    else
      who = User.find(params[:follow][:follower_id])
      whom = User.find(params[:follow][:person_id])
      redirect_to root_path if who != current_user

      if Follower.exists?(follower_id: who.id, person_id: whom.id)
        Follower.find_by(follower_id: who.id, person_id: whom.id).destroy
      else
        Follower.create(follower_id: who.id, person_id: whom.id)
      end
      
      redirect_to :back
    end
  end
end
