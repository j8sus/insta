class UsersController < ApplicationController
  def index
  	@users = User.all
  end

  def show
  	@user = User.find(params[:id])  	
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    redirect_to root_path if current_user != @user

    if @user.update(user_params)
      redirect_to user_path(@user) 
    else
      render 'edit'
    end    
  end

  private

  def user_params
  	params.require(:user).permit(:name, :surname, :image)
  end
end
