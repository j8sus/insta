class LikesController < ApplicationController
  def create
    unless user_signed_in?
      redirect_to new_user_session_path
    else
      photo = Photo.find(params[:like][:photo_id])
      user = User.find(params[:like][:user_id])
      redirect_to root_path if user != current_user

      if Like.exists?(user: user, photo: photo)
        Like.find_by(user: user, photo: photo).destroy
      else
        Like.create(user: user, photo: photo)
      end
      
      redirect_to :back
    end
  end
end
