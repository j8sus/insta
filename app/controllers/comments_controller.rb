class CommentsController < ApplicationController
  def create
    unless user_signed_in?
      redirect_to new_user_session_path
    else
      photo = Photo.find(params[:comment][:photo_id])
      user = User.find(params[:comment][:user_id])

      redirect_to root_path if user != current_user

      user.comments.create(comment_params)
      redirect_to user_path(photo.user)
    end
  end

  private

  def comment_params
  	params.require(:comment).permit(:body, :user_id, :photo_id)
  end
end
