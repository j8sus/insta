class PhotosController < ApplicationController
  def index
    unless user_signed_in?
      redirect_to new_user_session_path
    else
      @collection = []
      current_user.following.each do |following|
        user = User.find(following.person_id)
        @collection << user.photos
      end
    end
  end

  def create
  	@user = current_user

  	if @user.photos.create(photo_params)
  		redirect_to @user, notice: 'Photo was successfully created.'
  	else
  		redirect_to @user, notice: 'Smthng wrong.'
  	end
  end

  private

  def photo_params
  	params.require(:photo).permit(:image)	
  end
end
