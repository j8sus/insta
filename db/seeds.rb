# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

5.times do
	user = User.create!(
		name: Faker::Name.first_name,
		surname: Faker::Name.last_name,
		image: Faker::Avatar.image,
		email: Faker::Internet.email,
		password: Faker::Number.number(8)
	)
	3.times do
		user.photos.create!(
			image: Faker::Avatar.image
		)
	end
end

(1..20).each do |i|
	who  = rand(1..5)
	whom = rand(1..5)
	next if Follower.exists?(follower_id: who, person_id: whom) || who == whom

	Follower.create!(follower_id: who, person_id: whom)
end

(1..40).each do |i|
	Comment.create!(
		body: Faker::Lorem.sentence,
		user_id: rand(1..5),
		photo_id: rand(1..15)
	)
end

(1..40).each do |i|
	user_id = rand(1..5)
	photo_id = rand(1..15)
	next if Like.exists?(user_id: user_id, photo_id: photo_id)
	Like.create!(
		user_id: user_id,
		photo_id: photo_id
	)
end
