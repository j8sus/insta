Rails.application.routes.draw do
	root 'users#index'
  devise_for :users
  resources :users
  resources :photos, only: [:index, :create]
  resources :comments, only: :create
  resources :likes, only: :create
  resources :followers, only: :create
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
